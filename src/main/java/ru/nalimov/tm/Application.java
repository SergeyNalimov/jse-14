package ru.nalimov.tm;


import ru.nalimov.tm.controller.ProjectController;
import ru.nalimov.tm.controller.SystemController;
import ru.nalimov.tm.controller.TaskController;
import ru.nalimov.tm.controller.UserController;
import ru.nalimov.tm.enumerated.Role;
import ru.nalimov.tm.repository.ProjectRepository;
import ru.nalimov.tm.repository.TaskRepository;
import ru.nalimov.tm.repository.UserRepository;
import ru.nalimov.tm.service.ProjectService;
import ru.nalimov.tm.service.ProjectTaskService;
import ru.nalimov.tm.service.TaskService;
import ru.nalimov.tm.service.UserService;

import java.util.*;

import static ru.nalimov.tm.constant.TerminalConst.*;



/*Создали через Apache Maven + цикл while (с условием)*/

public class Application {

    private final ProjectRepository projectRepository = new ProjectRepository();

    private final TaskRepository taskRepository = new TaskRepository();

    private final UserRepository userRepository = new UserRepository();

    private final ProjectService projectService = new ProjectService(projectRepository, taskRepository);

    private final UserService userService = new UserService(userRepository);

    private final ProjectController projectController = new ProjectController(projectService, userService);

    private final UserController userController = new UserController(userService);

    private final TaskService taskService = new TaskService(taskRepository);

    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final TaskController taskController = new TaskController(taskService, projectTaskService, userService, projectService);

    private final SystemController systemController = new SystemController();

    public static Long id = null;

    public static Long userIdCurrent = null;

    {
        projectRepository.create("DEMO_PROJECT_3", "DESC PROJECT 3", userService.findByLogin("ADMIN").getId());
        projectRepository.create("DEMO_PROJECT_2", "DESC PROJECT 2", userService.findByLogin("TEST").getId());
        taskRepository.create("TEST_TASK_2", "DESC TASK 2", userService.findByLogin("TEST").getId());
        taskRepository.create("TEST_TASK_1", "DESC TASK 1", userService.findByLogin("TEST").getId());
        userService.create("user1","pass1","Фам1","Имя1","Отч1");
        userService.create("user2","pass2","Фам2","Имя2","Отч2",Role.ADMIN);

    }

    public static void main(final String[] args) {
        final Application app = new Application();
        final Scanner scanner = new Scanner(System.in);
        app.run(args);
        app.systemController.displayWelcome();
        String command = "";
       while (!EXIT.equals(command)) {
           command = scanner.nextLine();
           app.run(command);
       }
    }

    private void run(final String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    private int run(final String param) {
    if (param == null || param.isEmpty()) return -1;
        systemController.addCommandToHistory(param);
    switch (param) {
            case VERSION: return systemController.displayVersion();
            case ABOUT: return systemController.displayAbout();
            case HELP: return systemController.displayHelp();
            case EXIT: return systemController.displayExit();
            case COMMAND_HISTORY: return systemController.displayHistory();


            case PROJECT_CREATE: return projectController.createProject();
            case PROJECT_CLEAR: return projectController.clearProject();
            case PROJECT_LIST: return projectController.listProject();
            case PROJECT_VIEW_BY_INDEX: return projectController.viewProjectByIndex();
            case PROJECT_VIEW_BY_ID: return projectController.viewProjectById();
       //     case PROJECT_REMOVE_BY_NAME: return projectController.removeProjectByName();
            case PROJECT_REMOVE_BY_ID: return projectController.removeProjectById();
            case PROJECT_REMOVE_BY_INDEX: return projectController.removeProjectByIndex();
            case PROJECT_UPDATE_BY_INDEX: return projectController.updateProjectByIndex();
            case PROJECT_UPDATE_BY_ID: return projectController.updateProjectById();

            case TASK_CREATE: return taskController.createTask();
            case TASK_CLEAR: return taskController.clearTask();
            case TASK_LIST: return taskController.listTask();
            case TASK_VIEW_BY_INDEX: return taskController.viewTaskByIndex();
            case TASK_VIEW_BY_ID: return taskController.viewTaskById();
            case TASK_REMOVE_BY_ID: return taskController.removeTaskById();
        //    case TASK_REMOVE_BY_NAME: return taskController.removeTaskByName();
            case TASK_REMOVE_BY_INDEX: return taskController.removeTaskByIndex();
            case TASK_UPDATE_BY_INDEX: return taskController.updateTaskByIndex();
            case TASK_UPDATE_BY_ID: return taskController.updateTaskById();
            case TASK_ADD_PROJECT_BY_IDS: return taskController.addTaskToProjectByIds();
            case TASK_REMOVE_PROJECT_BY_IDS: return taskController.removeTaskProjectByIds();
            case TASK_LIST_BY_PROJECT_ID: return taskController.listTaskByProjectId();

            case TASK_ADD_TO_USER_BY_IDS: return taskController.addTaskToUser();
            case TASK_CLEAR_BY_USER_ID: return taskController.clearTasksByUserId();
            case TASK_LIST_BY_USER_ID: return taskController.viewTaskListByUserId();


            case USER_CREATE: return userController.createUser();
            case ADMIN_CREATE: return userController.createUser(Role.ADMIN);
            case USERS_CLEAR: return userController.clearUsers();
            case USERS_LIST: return userController.listUsers();
            case USER_VIEW_BY_LOGIN: return userController.viewUserByLogin();
            case USER_REMOVE_BY_LOGIN: return userController.removeUserByLogin();
            case USER_UPDATE_BY_LOGIN: return userController.updateUserByLogin();
            case USER_VIEW_BY_ID: return userController.viewUserById();
            case USER_REMOVE_BY_ID: return userController.removeUserById();
            case USER_UPDATE_BY_ID: return userController.updateUserById();
            case USER_AUTHENTIC: return userController.userAuthentic();
            case USER_DEAUTHENTIC: return userController.userDeauthentic();
            case USER_CHANGE_PASSWORD: return userController.userChangePassword();
            case USER_UPDATE_ROLE: return userController.updateRole(id);
            case USER_PROFILE_UPDATE: return userController.updateProfile(id);
            case USER_PROFILE_VIEW: return userController.userProfile(id);
            case PROJECT_ADD_TO_USER_BY_IDS: return projectController.addProjectToUser(id);
            case PROJECT_CLEAR_BY_USER_ID: return projectController.clearProjectsByUserId(id);
            case PROJECT_LIST_BY_USER_ID: return projectController.viewProjectListByUserId(id);
        default: return systemController.displayErr();
        }
    }

}
