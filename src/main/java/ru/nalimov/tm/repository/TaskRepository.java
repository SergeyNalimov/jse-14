package ru.nalimov.tm.repository;

import ru.nalimov.tm.entity.Task;

import java.util.*;

public class TaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    private final HashMap<String, List<Task>> tasksName = new HashMap<>();

    public Task create(final String name, final String description, final Long userId) {
        final Task task = new Task(name);
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        tasks.add(task);
        List<Task> tasksHashMap = tasksName.get(task.getName());
        if (tasksHashMap == null) tasksHashMap = new ArrayList<>();
        tasksHashMap.add(task);
        tasksName.put(task.getName(), tasksHashMap);
        return task;
    }


    public Task update(final Long id, final String name, final String description) {
        final Task task = findById(id);
        if (task == null) return null;
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    public Task findByIndex(final int index) {
        if (index < 0 || index > tasks.size() -1) return null;
        return tasks.get(index);
    }

    public List<Task> findByName(final String name) {
        final List<Task> tasks = new ArrayList<>();
        if (tasksName.get(name) == null) return null;
        for (final Task task: tasksName.get(name)) {
            tasks.add(task);

        }
        return tasks;
    }

    public Task removeById(final Long id) {
        final Task task = findById(id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    public Task removeByIndex(final int index) {
        final Task task = findByIndex(index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    public Task findById(final Long id) {
        if (id == null) return null;
        for (final Task task: tasks ) {
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    public Task findByProjectIdAndId(final Long projectId, final Long id) {
        if (id == null) return null;
        for (final Task task: tasks ) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (!idProject.equals(projectId)) continue;
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    public List<Task> findAddByProjectId(final Long projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task: findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (idProject.equals(projectId)) result.add(task);
        }
        return result;
    }

    public Task findByUserIdAndId(final Long id, final Long userId) {
        if (id == null)
            return null;
        for (final Task task : tasks) {
            final Long idUser = task.getUserId();
            if (idUser == null)
                continue;
            if (!idUser.equals(userId))
                continue;
            if (task.getId().equals(id))
                return task;
        }
        return null;
    }

    public List<Task> findAllByProjectId(final Long projectId) {
        final List<Task> tasksWithProject = new ArrayList<>();
        for (final Task task : findAll()) {
            if (task.getProjectId() != null && task.getProjectId().equals(projectId))
                tasksWithProject.add(task);
        }
        return tasksWithProject;
    }

    public List<Task> findAllByUserId(final Long userId) {
        final List<Task> tasksWithUser = new ArrayList<>();
        for (final Task task : findAll()) {
            if (task.getUserId() != null && task.getUserId().equals(userId))
                tasksWithUser.add(task);
        }
        return tasksWithUser;
    }

    public void clearTasksByUserId(final Long userId) {
        final List<Task> tasks = findAllByUserId(userId);
        if (tasks.isEmpty())
            return;
        for (Task task : tasks) {
            removeById(task.getId());
        }
    }

    public void clear() {
        tasks.clear();
    }

    public List<Task> findAll() { return tasks; }

}
