package ru.nalimov.tm.service;

import ru.nalimov.tm.Application;
import ru.nalimov.tm.entity.Project;
import ru.nalimov.tm.repository.ProjectRepository;
import ru.nalimov.tm.repository.TaskRepository;

import java.util.Collections;
import java.util.List;

public class ProjectService {

    private final ProjectRepository projectRepository;

    private final TaskRepository taskRepository;

    public ProjectService(ProjectRepository projectRepository, TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    public Project create(String name, String description, Long userid) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.create(name, description, userid);
    }

    public Project update(Long id, String name, String description) {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.update(id, name, description);
    }

    public Project findByIndex(int index) {
        if (index < 0) return null;
        return projectRepository.findByIndex(index);
    }

    public Project findById(Long id) {
        if (id == null) return null;
        return projectRepository.findById(id);
    }

    public Project removeById(Long id) {
        if (id == null) return null;
        return projectRepository.removeById(id);
    }

    public Project removeByIndex(int index) {
        if (index < 0 || index > projectRepository.size() -1) return null;
        if (projectRepository.findByIndex(index) == null) return null;
        if (projectRepository.findByIndex(index).getUserId().equals(Application.userIdCurrent)) return projectRepository.removeByIndex(index);
        else return null;

    }

    public void clear() {
        projectRepository.clear();
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public Project addProjectToUser(final Long projectId, final Long userId) {
        final Project project = projectRepository.findById(projectId);
        if (project == null)
            return null;
        project.setUserId(userId);
        return project;
    }

    public void clearProjectsByUserId(Long userId) {
        projectRepository.clearProjectsByUserId(userId);
    }

    public List<Project> findAllByUserId(Long userId) {
        return projectRepository.findAllByUserId(userId);
    }

    public void ProjectSortByName(List<Project> projects) {
        Collections.sort(projects, Project.ProjectSortByName);
    }

}
