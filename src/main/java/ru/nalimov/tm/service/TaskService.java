package ru.nalimov.tm.service;

import ru.nalimov.tm.entity.Task;
import ru.nalimov.tm.repository.TaskRepository;

import java.util.*;

public class TaskService {

    private final TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public Task create(String name, String description, Long userid) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepository.create(name, description, userid);
    }

    public Task update(Long id, String name, String description) {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepository.update(id, name, description);
    }

    public Task findByIndex(int index) {
        if (index < 0) return null;
        return taskRepository.findByIndex(index);
    }

    public Task removeById(Long id) {
        if (id == null) return null;
        return taskRepository.removeById(id);
    }

    public Task removeByIndex(int index) {
        if (index < 0) return null;
        return taskRepository.removeByIndex(index);
    }

    public Task findById(Long id) {
        if (id == null) return null;
        return taskRepository.findById(id);
    }

    public void clear() {
        taskRepository.clear();
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public List<Task> findAllByProjectId(Long projectId) {
        if (projectId == null) return null;
        return taskRepository.findAddByProjectId(projectId);
    }


    public Task addTaskToUser(Long taskId, Long userId) {
        if (taskId == null)
            return null;
        if (userId == null)
            return null;
        final Task task = taskRepository.findById(taskId);
        if (task == null)
            return null;
        task.setUserId(userId);
        return task;
    }

    public void clearTasksByUserId(Long userId) {
        if (userId == null)
            return;
        taskRepository.clearTasksByUserId(userId);
    }

    public List<Task> findAllByUserId(Long userId) {
        if (userId == null)
            return null;
        return taskRepository.findAllByUserId(userId);
    }

    public List<Task> TaskSortByName(List<Task> tasks) {
        Collections.sort(tasks, Task.TaskSortByName);
        return tasks;
    }

}
